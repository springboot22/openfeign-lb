package ribbon.ribbon.feign;

import feign.RequestLine;
import java.net.URI;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author will.tuo
 * @date 2022/12/15 11:23
 * @description 无
 */
@FeignClient(value = "server-c")
public interface ServiceC {

    @RequestLine("GET /api/web/a/get")
    String getA(URI uri);
}
