package ribbon.ribbon.feign;

import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author will.tuo
 * @date 2022/12/15 9:30
 * @description 无
 */
@FeignClient(value = "server-a")
public interface ServiceA {

    @RequestLine("GET /api/web/a/get")
    String getA();
}
