package ribbon.openfeigntest.ribbon;

import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义ribbon配置
 *
 * @author lihe
 * @date 2021/08/20
 */
@Configuration
public class CustomRibbonConfiguration {

    @Bean
    public IRule grayRule() {
        // 默认使用灰度路由策略
        return new GrayRule();
    }
}
