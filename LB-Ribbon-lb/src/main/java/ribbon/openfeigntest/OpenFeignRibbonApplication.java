package ribbon.openfeigntest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.openfeign.EnableFeignClients;
import ribbon.openfeigntest.ribbon.CustomRibbonConfiguration;

@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@RibbonClients(value = {
        @RibbonClient(value = "server-a", configuration = CustomRibbonConfiguration.class)
}, defaultConfiguration = CustomRibbonConfiguration.class)
//@RibbonClients(defaultConfiguration = CustomRibbonConfiguration.class)
public class OpenFeignRibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeignRibbonApplication.class, args);
    }

}
