package ribbon.openfeigntest.feign;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.Target;
import feign.Target.HardCodedTarget;
import feign.codec.Decoder;
import feign.codec.Encoder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.SpringClientFactory;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.ribbon.CachingSpringLoadBalancerFactory;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author will.tuo
 * @date 2022/12/9 15:38
 * @description 无
 */
@Data
@Configuration
@Import(FeignClientsConfiguration.class)
public class FeignService {

    private ServiceB serviceB;
    //    @Autowired
    private ServiceA serviceA;

    private ServiceC serviceC;

    public LoadBalancerFeignClient client(CachingSpringLoadBalancerFactory lbClientFactory,
            SpringClientFactory clientFactory) {
        return new LoadBalancerFeignClient(new Client.Default(null, null), lbClientFactory, clientFactory);
    }

    /**
     * autowire的就可以，创建的不行，应该还是要重新给他new一个client
     *
     * @param decoder
     * @param encoder
     */
    @Autowired
    public FeignService(Decoder decoder, Encoder encoder, CachingSpringLoadBalancerFactory lbClientFactory,
            SpringClientFactory clientFactory) {
        serviceA = Feign.builder().encoder(encoder).decoder(decoder).contract(new Contract.Default())
                .client(client(lbClientFactory, clientFactory))
                .target(new HardCodedTarget<>(ServiceA.class, "server-a", "http://server-a"));
        serviceB = Feign.builder().encoder(encoder).decoder(decoder).contract(new Contract.Default())
                .client(client(lbClientFactory, clientFactory))
                .target(new HardCodedTarget<>(ServiceB.class, "server-b", "http://server-b"));
        serviceC = Feign.builder().encoder(encoder).decoder(decoder)
                .target(Target.EmptyTarget.create(ServiceC.class));
    }
}
