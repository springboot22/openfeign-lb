package ribbon.ribbon.feign;

import feign.Client;
import feign.Feign;
import feign.Target;
import feign.Target.HardCodedTarget;
import feign.codec.Decoder;
import feign.codec.Encoder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.loadbalancer.FeignBlockingLoadBalancerClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author will.tuo
 * @date 2022/12/9 15:38
 * @description 无
 */
@Data
@Configuration
@org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient(value = "server-a", configuration = FeignLoadBalancerConfig.class)
@Import(FeignClientsConfiguration.class)
public class FeignService {

    private ServiceB serviceB;

    //    @Autowired
    private ServiceA serviceA;

    private ServiceC serviceC;

//    private LoadBalancerClient loadBalancerClient;
//
//    private LoadBalancerClientFactory loadBalancerClientFactory;
//@Autowired
//    public void setLoadBalancerClient(LoadBalancerClient loadBalancerClient) {
//        this.loadBalancerClient = loadBalancerClient;
//    }
//    @Autowired
//    public void setLoadBalancerClientFactory(
//            LoadBalancerClientFactory loadBalancerClientFactory) {
//        this.loadBalancerClientFactory = loadBalancerClientFactory;
//    }

    private Client client;

    public Client feignBlockingLoadBalancerClient(LoadBalancerClient loadBalancerClient,
            LoadBalancerClientFactory loadBalancerClientFactory) {
        return new FeignBlockingLoadBalancerClient(new Client.Default(null, null), loadBalancerClient,
                loadBalancerClientFactory);
    }

    @Autowired
    public FeignService(Decoder decoder, Encoder encoder, LoadBalancerClient loadBalancerClient,
            LoadBalancerClientFactory loadBalancerClientFactory) {
        serviceB = Feign.builder().encoder(encoder).decoder(decoder)
                .client(feignBlockingLoadBalancerClient(loadBalancerClient, loadBalancerClientFactory))
                .target(new HardCodedTarget<ServiceB>(ServiceB.class, "server-b", "http://server-b"));
        serviceA = Feign.builder().encoder(encoder).decoder(decoder)
                .client(feignBlockingLoadBalancerClient(loadBalancerClient, loadBalancerClientFactory))
//                .target(new HardCodedTarget<ServiceA>(ServiceA.class, "server-a", "http://server-a"));
                .target(Target.EmptyTarget.create(ServiceA.class));
        serviceC = Feign.builder().encoder(encoder).decoder(decoder)
                .target(Target.EmptyTarget.create(ServiceC.class));
    }


}
