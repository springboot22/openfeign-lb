package ribbon.ribbon.feign;

import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author will.tuo
 * @date 2022/12/15 11:23
 * @description 无
 */
@FeignClient(value = "server-b")
public interface ServiceB {

    @RequestLine("GET /api/web/a/get")
    String getA();
}
