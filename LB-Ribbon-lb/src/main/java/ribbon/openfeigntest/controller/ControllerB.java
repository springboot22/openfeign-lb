package ribbon.openfeigntest.controller;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ribbon.openfeigntest.feign.FeignService;

@RestController
@RequestMapping("/api/web/b")
public class ControllerB {

    @Autowired
    FeignService feignService;

    @GetMapping("/get")
    public String get() {
        return feignService.getServiceB().getA();
    }

    @GetMapping("/geta")
    public Object geta() {
        return feignService.getServiceA().getA();
    }

    @GetMapping("/geta2")
    public Object geta2() throws URISyntaxException {
        return feignService.getServiceC().getA(new URI("http://127.0.0.1:8082"));
    }
}
