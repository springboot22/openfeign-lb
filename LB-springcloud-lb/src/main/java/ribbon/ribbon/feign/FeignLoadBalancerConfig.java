package ribbon.ribbon.feign;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cloud.loadbalancer.core.ReactorServiceInstanceLoadBalancer;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.context.annotation.Bean;

/**
 * @author will.tuo
 * @date 2022/12/15 9:34
 * @description 无
 */
//@Configuration
public class FeignLoadBalancerConfig {

    // 参数 serviceInstanceListSupplierProvider 会自动注入
//    @Bean
//    public ReactorServiceInstanceLoadBalancer customLoadBalancer(Environment environment,
//            LoadBalancerClientFactory loadBalancerClientFactory) {
//        //修改这里指向自定义的配置文件
//        String name = environment.getProperty(LoadBalancerClientFactory.PROPERTY_NAME);
//        return new GrayRuleLoadBalancer(
//                loadBalancerClientFactory.getLazyProvider(name, ServiceInstanceListSupplier.class));
//    }

    @Bean
    public ReactorServiceInstanceLoadBalancer customLoadBalancer(
            ObjectProvider<ServiceInstanceListSupplier> serviceInstanceListSupplierProvider) {
        //修改这里指向自定义的配置文件
//        String name = environment.getProperty(LoadBalancerClientFactory.PROPERTY_NAME);
        return new GrayRuleLoadBalancer(serviceInstanceListSupplierProvider);
    }


}
