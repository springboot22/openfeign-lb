package ribbon.openfeigntest.ribbon;

import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.Server.MetaInfo;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 灰度路由策略（基于对ribbon自身的轮询策略改造而来）
 *
 * @author lihe
 * @date 2021/08/25
 */
public class GrayRule extends AbstractLoadBalancerRule {

    private static final Logger log = LogManager.getLogger(GrayRule.class);

    /**
     * 非灰度的计数器
     */
    private AtomicInteger nextServerCyclicCounter4Stable;

    /**
     * 灰度的计数器
     */
    private AtomicInteger nextServerCyclicCounter4Gray;

    /**
     * 所有实例（灰度 + 非灰度）的计数器
     */
    private AtomicInteger nextServerCyclicCounter4All;

    /**
     * 服务实例标识：灰度实例
     */
    private static final String SERVER_FLAG_GRAY = "gray";

    /**
     * 服务实例标识：稳定实例
     */
    private static final String SERVER_FLAG_STABLE = "stable";

    /**
     * 服务实例标识：所有实例
     */
    private static final String SERVER_FLAG_ALL = "all";

    public GrayRule() {
        nextServerCyclicCounter4Stable = new AtomicInteger(0);
        nextServerCyclicCounter4Gray = new AtomicInteger(0);
        nextServerCyclicCounter4All = new AtomicInteger(0);
    }

    public GrayRule(ILoadBalancer lb) {
        this();
        setLoadBalancer(lb);
    }

    /**
     * 灰度路由：先对服务进行灰度过滤，找出符合要求的服务实例， 如果没有灰度实例，则按用户配置进行负载均衡，有的话按则ribbon的轮询进行
     *
     * @param lb
     * @param key
     * @return
     */
    private Server choose(ILoadBalancer lb, Object key) {
        log.info("进入我的Rule");
        if (lb == null) {
            log.warn("[feign gray extension] no load balancer");
            return null;
        }
        String userId = this.getHeader("UserId");
        Server server = null;
        int count = 0;
        while (server == null && count++ < 10) {
            // 首先对服务进行灰度过滤，找出符合要求的服务实例
            GrayFilterResult reachableServersFilterResult = this.grayFilter(lb.getReachableServers());
            GrayFilterResult allServersFilterResult = this.grayFilter(lb.getAllServers());
            List<Server> reachableServers = reachableServersFilterResult.getResultServers();
            List<Server> allServers = allServersFilterResult.getResultServers();
            int upCount = reachableServers.size();
            int serverCount = allServers.size();

            if ((upCount == 0) || (serverCount == 0)) {
                log.warn(
                        "[feign gray extension] finally return null, No up servers available from load balancer, lb={}, userId={}",
                        lb,
                        userId);
                return null;
            }
            // 灰度、非灰度、所有服务的轮询分离
            AtomicInteger nextServerCyclicCounter;
            switch (allServersFilterResult.getServerFlag()) {
                case SERVER_FLAG_ALL:
                    nextServerCyclicCounter = nextServerCyclicCounter4All;
                    break;
                case SERVER_FLAG_STABLE:
                    nextServerCyclicCounter = nextServerCyclicCounter4Stable;
                    break;
                case SERVER_FLAG_GRAY:
                    nextServerCyclicCounter = nextServerCyclicCounter4Gray;
                    break;
                default:
                    nextServerCyclicCounter = nextServerCyclicCounter4All;
                    break;
            }
            int nextServerIndex = incrementAndGetModulo(serverCount, nextServerCyclicCounter);
            server = allServers.get(nextServerIndex);
            if (server == null) {
                /* Transient. */
                Thread.yield();
                continue;
            }

            if (server.isAlive() && (server.isReadyToServe())) {

                return server;
            }

            // Next.
            server = null;
        }

        if (count >= 10) {
            log.warn(
                    "[feign gray extension] finally return null, No available alive servers after 10 tries from load balancer, lb={}, userId={}",
                    lb, userId);
        }
        return server;
    }

    /**
     * Inspired by the implementation of {@link AtomicInteger#incrementAndGet()}.
     *
     * @param modulo The modulo to bound the value of the counter.
     * @return The next value.
     */
    private int incrementAndGetModulo(int modulo, AtomicInteger nextServerCyclicCounter) {
        for (; ; ) {
            int current = nextServerCyclicCounter.get();
            int next = (current + 1) % modulo;
            if (nextServerCyclicCounter.compareAndSet(current, next)) {
                return next;
            }
        }
    }

    /**
     * 过滤出符合灰度策略的服务器
     *
     * @param servers
     * @return
     */
    private GrayFilterResult grayFilter(List<Server> servers) {
        String userId = this.getHeader("UserId");
        // 过滤逻辑：如果有灰度版本服务实例，则返回灰度的，没有的话返回所有
        if (CollectionUtils.isEmpty(servers)) {
            log.warn(
                    "[feign gray extension] gray server filter: find all server, because param servers is empty, userId={}",
                    userId);
            return new GrayFilterResult(SERVER_FLAG_ALL, servers);
        }
        // 服务名称
        // 灰度版本号
        String grayVersion = this.getHeader("targetVersion");
        if (StringUtils.isEmpty(grayVersion)) {
            List<Server> stableServers = this.findServer(false, grayVersion, servers);

            return new GrayFilterResult(SERVER_FLAG_STABLE, stableServers);
//            return new GrayFilterResult(SERVER_FLAG_ALL, servers);
        }
        // 请求模式：stable、gray、all，具体含义见后面switch注释
        String requestMode = this.getHeader("requestMode");
        if (StringUtils.isEmpty(requestMode)) {

            return new GrayFilterResult(SERVER_FLAG_ALL, servers);
        }
        GrayFilterResult result;
        switch (requestMode) {
            case "stable":
                // 只走稳定版实例（灰度测试阶段且是非灰度用户的请求）
                List<Server> stableServers = this.findServer(false, grayVersion, servers);

                result = new GrayFilterResult(SERVER_FLAG_STABLE, stableServers);
                break;
            case "gray":
                // 只走灰度实例（灰度测试阶段且是灰度用户的请求）
                List<Server> grayServers = this.findServer(true, grayVersion, servers);
                if (CollectionUtils.isEmpty(grayServers)) {
                    // 没有灰度实例，走稳定实例

                    result = new GrayFilterResult(SERVER_FLAG_STABLE, servers);
                } else {
                    // 有灰度实例

                    result = new GrayFilterResult(SERVER_FLAG_GRAY, grayServers);
                }
                break;
            case "all":
                // 走所有实例（滚动发布阶段）

                result = new GrayFilterResult(SERVER_FLAG_ALL, servers);
                break;
            default:

                result = new GrayFilterResult(SERVER_FLAG_ALL, servers);
                break;
        }
        return result;
    }

    /**
     * 根据条件找出对应的服务实例
     *
     * @param gray
     * @param grayVersion
     * @param servers
     * @return
     */
    private List<Server> findServer(boolean gray, String grayVersion, List<Server> servers) {
        servers.forEach(server -> {

            MetaInfo metaInfo = server.getMetaInfo();
            NacosServer nacosServer = (NacosServer) server;
            String grayTag = nacosServer.getMetadata().get("grayTag");
            log.info("safdasdfsdsdfsda" + grayTag);

        });
        return servers;
    }

    /**
     * 从请求头获取指定信息
     *
     * @return
     */
    private String getHeader(String headerName) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes == null) {
            // 获取不到原请求，可能是对feign的调用为异步导致
            log.error(
                    "[feign gray extension] gray server filter failed: RequestContextHolder.getRequestAttributes() is null");
            return null;
        }
        HttpServletRequest oldRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
        return oldRequest.getHeader(headerName);
    }

    @Override
    public Server choose(Object key) {
        return choose(getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {
        // 继续需要重写的方法，没有实际用处
    }

    /**
     * 服务实例灰度过滤结果
     *
     * @author lihe
     * @date 2021/08/25
     */
    private class GrayFilterResult {

        /**
         * 服务实例标识：gray（灰度）、stable（稳定）、all（所有）
         */
        private String serverFlag;

        /**
         * 过滤结果
         */
        private List<Server> resultServers;

        /**
         * @param grayFlag
         * @param resultServers
         */
        public GrayFilterResult(String serverFlag, List<Server> resultServers) {
            super();
            this.serverFlag = serverFlag;
            this.resultServers = resultServers;
        }

        /**
         * @return the serverFlag
         */
        public String getServerFlag() {
            return serverFlag;
        }

        /**
         * @return the resultServers
         */
        public List<Server> getResultServers() {
            return resultServers;
        }
    }
}
